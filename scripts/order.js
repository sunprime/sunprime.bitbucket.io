class Order extends Component{
    constructor(sSelector){
        super(sSelector);
        this.textfields = this.findObject(".b-order__textfield");
        this.name   = this.findObject(".b-order__textfield_name");
        this.email  = this.findObject(".b-order__textfield_email");
        this.tel    = this.findObject(".b-order__textfield_tel");
        this.goods  = this.findObject(".b-order__textfield_goods");
        this.message = this.findObject(".b-order__message");
        this.messageError = this.findObject((".b-form__message"));

        this.createEvents();
    }

    showHideOrderForm(event){
        //event.preventDefault();
        this.elem.find(".b-order").slideToggle();
    }

    send(event){
        event.preventDefault();
        if (!this.checkTextfields(event.currentTarget)){
            $.ajax({
                'url'     : 'order.php', //url
                'method'  : 'POST', //����� �������� ������
                'dataType': 'json', //��� ������ ������������ � callback �������
                'timeout' : 1000, //����� ������� � �������������
                //������, ������� �������� �� ������
                'data'    : {
                    'name' : this.name.val(),
                    'email': this.email.val(),
                    'tel'  : this.tel.val(),
                    'goods': this.goods.val()
                },
                'complete': (oAjax) =>{
                    let jsonServerResponse = oAjax.responseJSON;
                    //console.log(oAjax);
                    if (oAjax.status == 200){//200 - OK
                        if (jsonServerResponse != undefined){
                            this.message.html(jsonServerResponse.message);
                        }
                        else {
                            alert(`response that cannot be parsed as JSON: ${oAjax.responseText}`);
                        }
                    }
                    else if(oAjax.status == 404){
                        alert('AJAX backend is not found.');
                    }
                    else if(oAjax.statusText == 'timeout'){
                        alert('AJAX request has timed out.');
                    }
                    else {
                        alert('Totally unpredicted error.');
                    }

                }
            });
            this.clearAll();
        }
        else {
            this.textfields.each((i, currenTextfield)=>{
                let textfield = $(currenTextfield);
                if (textfield.hasClass("b-textfield_error")){
                    textfield.val('');
                }
            });
        }
    }

    //������� ���� ����� �����, ����� �������� ������� �� ������
    clearAll(event){
        this.textfields.each((i, currentTextfield)=>{
            let textfield = $(currentTextfield);
            textfield.val("");
        });
        $.cookie("cartGoods", null);
        cart1.load();
    }

    //�������� ���������� ���� �� ������������ �����
    checkTextField(event, textfield){
        let currentTextfield    = textfield ? textfield : $(event.currentTarget),
            currentTextfieldVal = currentTextfield.val(),
            regexps             = {
                "name"        : "^[A-Za-z�-��-� \\-]{2,15}$", //���, ��� �����, �������, - �� 2 �� 15
                "email"       : "^\\S+@\\S+$",
                "telephone"       : "^(\\+)[0-9]{1,12}$", // ����� �� 1 �� 12
            },
            currentTextfieldName = currentTextfield.attr("name"),
            currentRegExp        = new RegExp(regexps[currentTextfieldName]),
            textfieldError       = !currentTextfieldVal.match(currentRegExp); //���� ���� ������, �� �������� �������� true
        currentTextfield.toggleClass("b-textfield_error", textfieldError);
        return textfieldError;
    }


    //�������� �� ������������ ���������� ���� �����
    checkTextfields(event){
        let formError = false;
        this.textfields.each((i, currentTextfield)=>{
            let textfield       = $(currentTextfield),
                textfieldError  = this.checkTextField(null, textfield);

            if (textfieldError){
                formError = true;
            }
        });
        let methodType = formError ? "slideDown" : "slideUp";
        //����������� �������� ������ ����� ����������
        this.messageError[methodType]();
        console.log(formError);
        return formError;
    }

    createEvents(){
        this.elem.find(".b-make__order").click(this.showHideOrderForm.bind(this));
        this.elem.submit(this.send.bind(this));
        this.textfields.blur(this.checkTextField.bind(this));
    }
}